using NUnit.Framework;
using System;
using TRPO_Kurs.lib;

namespace TRPO_Kurs.test
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            const double Revenue = 50000;
            const double StocksBegin = 100;
            const double StockEnd = 0;
            const double CreditDebtBegin = 50000;
            const double CreditDebEnd = 0;
            const double expected = 731.46;
            double result = new PP().PeriodOfBalance(Revenue,StocksBegin,StockEnd,CreditDebtBegin,CreditDebEnd);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Test2()
        {
            const double tProductProcessing = 200;
            const double tTechnologicalMaintenance = 22;
            const double tRest = 5;
            const double expected = 227;
            double result = new PP().ProductionCycle(tProductProcessing, tTechnologicalMaintenance, tRest);
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void Test3()
        {
            const double n = 5;
            const double m = 10;
            const double Ci = 10;
            const double ti = 20;
            const double expected = 100;
            double result = new PP().Posled(n, m, Ci, ti);
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void Test4()
        {
            const double n = 100;
            const double p = 5;
            const double m = 10;
            const double Ci = 10;
            const double ti = 20;
            const double expected = 290;
            double result = new PP().Paral(n,p, m, Ci, ti);
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void TrowTest1()
        {

            const double m = -1;
            const double n = -1;
            const double Ci = -1;
            const double ti = -1;

            Assert.Throws<ArgumentException>(() => new PP().Posled( m, n,Ci,ti));
        }
        [Test]
        public void TrowTest2()
        {

            const double m = -1;
            const double n = -1;
            const double p = -1;
            const double Ci = -1;
            const double ti = -1;

            Assert.Throws<ArgumentException>(() => new PP().Paral(m, p, n, Ci, ti));
        }
    }
}