﻿using System;

namespace TRPO_Kurs.lib
{
    public class PP
    {

        public double PeriodOfBalance(double Revenue, double StocksBegin, double StockEnd, double CreditDebtBegin, double CreditDebEnd)
        {
            double sredStocks = Revenue / (StocksBegin + StockEnd) / 2;
            double pSredStocks = 365 / sredStocks;
            double sredCreditDeb = Revenue / (CreditDebtBegin + CreditDebEnd) / 2;
            double pSredCreditDeb = 365 / sredCreditDeb;
            return pSredStocks + pSredCreditDeb;
        }
        public double ProductionCycle(double tProductProcessing, double tTechnologicalMaintenance, double tRest)
        {
            return tProductProcessing + tTechnologicalMaintenance + tRest; 
        }
        public double Posled(double n, double m, double Ci,double ti)
        {
            if (m > 11 || m <= 0 || n <= 0 || Ci < 0 || ti < 0) throw new ArgumentException();
            double sum = 0;
            for (int i = 0; i < m; i++)
                sum += (ti / Ci);
            return n * sum;
        }

        public double Paral(double n,double p, double m, double Ci, double ti)
        {
            if (m > 10 || m <= 0 || n <= 0 || Ci < 0 || ti < 0 || p < 0) throw new ArgumentException();
            double sum = 0;
            for (int i = 0; i < m; i++)
                sum += (ti / Ci);
            return (n - p) * ti / Ci + p * sum ;
        }
    }
}