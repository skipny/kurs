﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPO_Kurs.lib;

namespace TRPO_Kurs.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Текстик (*.txt)|*.txt";

            if (saveFileDialog1.ShowDialog() == true)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.OpenFile(), System.Text.Encoding.Default))
                {
                    sw.Write(txtLabael1.Content + "" + txtBox1.Text + "\n" + txtLabael2.Content + "" + txtBox2.Text + "\n" + txtLabael3.Content + "" + txtBox3.Text + "\n"
                        + txtLabael4.Content + "" + txtBox4.Text + "\n" + txtLabael5.Content + "" + txtBox5.Text + "\n" + txtLabael6.Content + "" + txtBox6.Text + "\n");
                    sw.Close();
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Текстик (*.txt)|*.txt";

            if (saveFileDialog1.ShowDialog() == true)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.OpenFile(), System.Text.Encoding.Default))
                {
                    sw.Write(txtLabael7.Content + "" + txtBox7.Text + "\n" + txtLabael8.Content + "" + txtBox8.Text + "\n" + txtLabael9.Content + "" + txtBox9.Text + "\n"
                        + txtLabael10.Content + "" + txtBox10.Text);
                    sw.Close();
                }
            }

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Текстик (*.txt)|*.txt";

            if (saveFileDialog1.ShowDialog() == true)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.OpenFile(), System.Text.Encoding.Default))
                {
                    sw.Write(txtLabael11.Content + "" + txtBox11.Text + "\n" + txtLabael12.Content + "" + txtBox12.Text + "\n" + txtLabael13.Content + "" + txtBox13.Text + "\n"
                        + txtLabael14.Content + "" + txtBox14.Text + "\n" + txtLabael15.Content + "" + txtBox15.Text);
                    sw.Close();
                }
            }

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Текстик (*.txt)|*.txt";

            if (saveFileDialog1.ShowDialog() == true)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.OpenFile(), System.Text.Encoding.Default))
                {
                    sw.Write(txtLabael16.Content + "" + txtBox16.Text + "\n" + txtLabael17.Content + "" + txtBox17.Text + "\n" + txtLabael18.Content + "" + txtBox18.Text + "\n"
                        + txtLabael19.Content + "" + txtBox19.Text + "\n" + txtLabael20.Content + "" + txtBox20.Text + "\n" + txtLabael21.Content + "" + txtBox21.Text + "\n");
                    sw.Close();
                }
            }

        }
    }

    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private double _A=1;
        public double A
        {
            get { return _A; }
            set{
                if (value <= -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _A = value;
                if (_A > -1)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res1)));
            }
        }

        private double _B=1;
        public double B
        {
            get { return _B; }
            set{
                if (value <= -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _B = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res1)));
            }
        }
        private double _C=1;
        public double C
        {
            get { return _C; }
            set{
                if (value <= -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _C = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res1)));
            }
        }

        private double _D=1;
        public double D{
            get { return _D; }
            set{
                if (value <= -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _D = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res1)));
            }
        }

        private double _E=1;
        public double E
        {
            get { return _E; }
            set{
                if (value <= -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _E = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res1)));
            }
        }

        private double _n=1;
        public double N
        {
            get { return _n; }
            set{
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно положительным");
                    return;
                }
                _n = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res2)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res3)));
            }
        }

        private double _m=1;
        public double M
        {
            get { return _m; }
            set{
                if (value <= 0 || value>10)
                {
                    MessageBox.Show("m должно быть > 0 и <=10");
                    return;
                }
                _m = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res2)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res3)));
            }
        }

        private double _Ci=1;
        public double Ci
        {
            get { return _Ci; }
            set{
                if (value <-1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _Ci = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res2)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res3)));
            }
        }

        private double _ti=1;
        public double Ti
        {
            get { return _ti; }
            set{
                if (value < -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _ti = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res2)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res3)));
            }
        }

        private double _p=1;
        public double P
        {
            get { return _p; }
            set{
                if (value < -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _p = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res3)));
            }
        }

        private double _Tp=1;
        public double Tp
        {
            get { return _Tp; }
            set
            {
                if (value < -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _Tp = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res4)));
            }
        }

        private double _To=1;
        public double To
        {
            get { return _To; }
            set
            {
                if (value < -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _To = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res4)));
            }
        }

        private double _Tn=1;
        public double Tn
        {
            get { return _Tn; }
            set
            {
                if (value < -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _Tn = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res4)));
            }
        }

        
        public double Res1
        {
            get { return new PP().PeriodOfBalance(A, B, C, D, E); }
        }

        public double Res2
        {
            get { return new PP().Posled(N,M,Ci,Ti); }
        }

        public double Res3
        {
            get { return new PP().Paral(N, P, M, Ci, Ti); }
        }

        public double Res4
        {
            get { return new PP().ProductionCycle(Tp, To, Tn); }
        }
    }
}
